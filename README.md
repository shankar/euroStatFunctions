## European Energy Datasheets Functions

This is a joint project aimed at developing read, convert and calculate functions for the European Energy Datasheets.

Raw public data can be found on the following link: https://ec.europa.eu/energy/en/data-analysis/energy-statistical-pocketbook

## Change-log

A detailed change-log for developments can be found [here](/docs/change_log.md)

## Authors

Atreya Shankar, Renato Rodrigues

Research Domain 3, Potsdam Institute for Climate Impact Research (PIK)
