## Developments

### Final output
* include units into final magpie object, perhaps as additional dimension
* include download script that intelligently checks timestamps of input data to advise data updating

### Robust mappings
* since European energy datasheets are updated frequently with new rows/classes/subclasses being added, it is wise to make our processing and data-grouping more robust
* we approach this by developing direct mappings from european datasheets to remind, which can be simply aggregated
* furthermore, we will develop a warning script which will prompt the user to update "to-remind" mappings based on changes in eurostat data
